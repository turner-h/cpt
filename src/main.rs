use rand::Rng;
use std::io::stdin;

const MAX_POSS_AGE: u8 = 128;

#[derive(Clone, Copy, Default)]
struct Chicken {
    current_age: u8,
    max_age: u8,
    gender: bool, //false = male, true = female
}

impl Chicken {
    pub fn new(current_age: u8, max_age: u8, gender: bool) -> Chicken {
        Self { current_age: current_age, max_age: max_age, gender: gender }
    }

    pub fn random() -> Chicken {
        let mut rng = rand::thread_rng();
        let max_age = rng.gen_range(MAX_POSS_AGE-20..MAX_POSS_AGE);
        Chicken::new(rng.gen_range(0..max_age), max_age, rng.gen_bool(0.5))
    }
}

fn init_chickens() -> Vec<Chicken> {
    let mut rng = rand::thread_rng();
    let starting_amount = rng.gen_range(0..100);
    let mut chickens = vec![Chicken::default(); starting_amount];
    for chicken in 0..starting_amount {
        chickens[chicken] = Chicken::random();
    }

    chickens
}

fn buy_chickens(num: u8, chickens: &mut Vec<Chicken>) -> &mut Vec<Chicken> {
    let mut chick_arr = vec![Chicken::default(); num.into()];
    for chicken in 0..num {
        let chick = Chicken::random();
        chick_arr[chicken as usize] = chick;
    }

    chickens.append(&mut chick_arr);
    chickens
}

fn sell_chickens(num: u8, chickens: &mut Vec<Chicken>) -> &mut Vec<Chicken> {
    for chicken in 0..num {
       chickens.remove(chicken as usize); 
    }

    chickens
}

fn chicken_ecosystem(chickens: &mut Vec<Chicken>) -> &mut Vec<Chicken> {
    let mut rng = rand::thread_rng();
    
    for chicken in 0..chickens.len() {
        if chickens[chicken + 1].gender != chickens[chicken].gender {
            if chickens[chicken + 1].current_age >= 40 && chickens[chicken].current_age >= 40 {
                let chick = Chicken::new(0, rng.gen_range(MAX_POSS_AGE-20..MAX_POSS_AGE), rng.gen_bool(0.5));
                let mut chick_arr = vec![Chicken::default(); 1];
                chick_arr[0] = chick;
                chickens.append(&mut chick_arr);
            }
        }

        if chickens[chicken].current_age >= chickens[chicken].max_age {
            chickens.remove(chicken);
        }

        chickens[chicken].current_age += 1;
    }

    chickens
}

fn display(chickens: &Vec<Chicken>, year: u8) {
    let mut male_chickens = 0;
    let mut female_chickens = 0;
    let mut chicken_age: u64 = 0;

    for chicken in chickens {
        if chicken.gender {
            female_chickens += 1;
        } else {
            male_chickens += 1;
        }

        chicken_age += chicken.current_age as u64;
    }

    println!("     __//");
    println!("    /.__.\\");
    println!(" '__/    \\");
    println!("  \\-      )");
    println!("   \\_____/");
    println!("_____|_|____");
    println!("     \" \"");
    
    println!("Year: {}", year);
    println!("Number of Chickens: {}", chickens.len());
    println!("Number of Male Chickens: {}", male_chickens);
    println!("Number of Female Chickens: {}", female_chickens);
    println!("Average Age of Chickens: {}", chicken_age / (chickens.len() as u64));
    println!("\nType buy 10, buy 100, sell 10, sell 100, or press enter.");
    
}

fn main() {
    let mut chickens = init_chickens();
    let mut input = String::from("");
    let mut year = 0;

    loop {
        print!("{esc}[2J{esc}[1;1H", esc = 27 as char); //clear screen

        match input.as_str() {
            "buy 10\n" => {
                chickens = buy_chickens(10, &mut chickens).to_vec();
                display(&chickens, year);
            },
            "buy 100\n" => {
                chickens = buy_chickens(100, &mut chickens).to_vec();
                display(&chickens, year);
            },
            "sell 10\n" => {
                chickens = sell_chickens(10, &mut chickens).to_vec();
                display(&chickens, year);
            },
            "sell 100\n" => {
                chickens = sell_chickens(100, &mut chickens).to_vec();
                display(&chickens, year);
            },
            _ => {
                chickens = chicken_ecosystem(&mut chickens).to_vec();
                year += 1;
                display(&chickens, year);
            },
        }

        input = String::from("");
        stdin().read_line(&mut input).unwrap();
    }
}