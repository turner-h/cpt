# Chicken Simulator

An application that simulates chickens.

### Features
- [x] A functional chicken ecosystem
- [x] A ``Purchase Chickens`` button to add chickens to the ecosystem
- [x] Chicken life cycle
- [x] Chicken struct with information on aformentioned chicken

### Why?
idk